package devils.demo.zatonskiy.activities;

import android.graphics.Point;
import android.media.MediaPlayer;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.widget.LinearLayout;


import devils.demo.zatonskiy.R;
import devils.demo.zatonskiy.dialog.GameOverDialog;
import devils.demo.zatonskiy.utils.MusicPlayer;
import devils.demo.zatonskiy.view.GameView;
import devils.demo.zatonskiy.utils.PrefManager;

public class GameActivity extends AppCompatActivity implements GameView.GameOverListener {

    private GameView pirateGameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PrefManager.resetPoints(this);
        PrefManager.resetLives(this);
        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);

        pirateGameView = new GameView(this, size.x, size.y);

        setContentView(R.layout.activity_game);

        setContentView(R.layout.activity_game);
        LinearLayout surface = (LinearLayout)findViewById(R.id.surface);
        surface.addView(pirateGameView);
    }

    //pausing the game when activity is paused
    @Override
    protected void onPause() {
        super.onPause();
        pirateGameView.pause();
       MusicPlayer.pause();
    }

    //running the game when activity is resumed
    @Override
    protected void onResume() {
        super.onResume();
        pirateGameView.resume();
        if (PrefManager.getMusic(this))
            MusicPlayer.get(this).start();
    }

    @Override
    public void onGameOver() {
        new GameOverDialog().show(getSupportFragmentManager(), "GameOverDialog");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MediaPlayer mediaPlayer = MusicPlayer.get(this);
        if (mediaPlayer != null && !mediaPlayer.isPlaying()) MusicPlayer.release();
    }
}
