package devils.demo.zatonskiy.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import devils.demo.zatonskiy.R;
import devils.demo.zatonskiy.utils.ServerDataModel;
import devils.demo.zatonskiy.view.Web;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static devils.demo.zatonskiy.utils.PrefManager.getAccessPermission;
import static devils.demo.zatonskiy.utils.PrefManager.getFirstStart;
import static devils.demo.zatonskiy.utils.PrefManager.setAccessPermission;
import static devils.demo.zatonskiy.utils.PrefManager.setFirstStart;


public class ServerConnection extends AppCompatActivity {

    public static String JSON_URL = "https://raw.githubusercontent.com/user333666999/SERVER_COMANDS/master/server.json";
    List<ServerDataModel> serverCommandsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        serverCommandsList = new ArrayList<>();

        GetData getData = new GetData();
        getData.execute();


        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (getAccessPermission(getApplicationContext())) {
                    startActivity(new Intent(ServerConnection.this, Web.class));
                } else startActivity(new Intent(ServerConnection.this, MainActivity.class));
            }
        };
        handler.postDelayed(runnable, 3000);





    }

    public class GetData extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String current = "";

            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(JSON_URL);
                urlConnection = (HttpURLConnection) url.openConnection();

                InputStream is = urlConnection.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);

                int data = isr.read();
                while (data != -1) {

                    current += (char) data;
                    data = isr.read();
                }
                return current;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }


            return current;
        }

        @Override
        protected void onPostExecute(String s) {

            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("commands");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    ServerDataModel model = new ServerDataModel();
                    model.setAccess(jsonObject1.getString("access"));
                    model.setRestart(jsonObject1.getString("restart"));

                    serverCommandsList.add(model);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (serverCommandsList.get(0).getRestart().contains("true"))
                setFirstStart(true, getApplicationContext());

            if (getFirstStart(getApplicationContext())) {
                setFirstStart(false, getApplicationContext());
                if (serverCommandsList.get(0).getAccess().contains("true"))
                    setAccessPermission(true, getApplicationContext());
                else setAccessPermission(false, getApplicationContext());
            }

        }
    }

}