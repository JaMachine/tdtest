package devils.demo.zatonskiy.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import devils.demo.zatonskiy.R;
import devils.demo.zatonskiy.adapter.UserScoresAdapter;

public class PersonalRecordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_record);

        RecyclerView recordsRV = findViewById(R.id.rv_records);
        recordsRV.setLayoutManager(new LinearLayoutManager(this));
        recordsRV.setAdapter(new UserScoresAdapter(this));
    }
}
