package devils.demo.zatonskiy.utils;

public class ServerDataModel {
    String access, restart;

    public ServerDataModel(){
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getRestart() {
        return restart;
    }

    public void setRestart(String restart) {
        this.restart = restart;
    }
}
